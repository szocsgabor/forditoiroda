﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Forditoiroda
{
    public partial class Doku : Form
    {
        public static string connString = $"server=localhost;user=gabor;database=iroda;port=3306;password=gabor";
        MySqlConnection conn = new MySqlConnection(connString);
        public Doku()
        {
            conn.Open();
            InitializeComponent();
        }
        private void ButtonDocuExit_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void ButtonDokuInsert_Click(object sender, EventArgs e)
        {
            int terjedelem = Convert.ToInt32(textBoxTerjed.Text);
            string szakterulet = Convert.ToString(comboBoxSzakter.SelectedItem);
            // a válaszott nyelv id-jának eltárolása. A kiválasztott combos stringet elteszem, ezt felsplittelem |-nél, az ID az első | előtt van:
            string valasztottnyelv = Convert.ToString(comboBoxNyelvid.SelectedItem);
            string[] darabvalasztottnyelv = valasztottnyelv.Split('|');
            int nyelvid = Convert.ToInt32(darabvalasztottnyelv[0]);
            int munkaido = Convert.ToInt32(textBoxMunkaido.Text);

            try
            {
                string rogzit = $"INSERT INTO doku (id, terjedelem, szakterulet, nyelvid, munkaido) values (null, '{terjedelem}','{szakterulet}','{nyelvid}','{munkaido}');";
                MySqlCommand insert = new MySqlCommand(rogzit, conn);
                insert.ExecuteNonQuery();
                MessageBox.Show("Rögzítve!", "Rögzítve", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBoxMunkaido.Clear();
                textBoxTerjed.Clear();
                comboBoxNyelvid.SelectedIndex = -1;
                comboBoxSzakter.SelectedIndex = -1;
            }
            catch (Exception error)
            {
                MessageBox.Show("A rögzítés sikertelen" + " " + error);
            }

        }
        private void Doku_Load(object sender, EventArgs e)
        {
            try
            {
                string comboszakter = $"select distinct(szakterulet) from doku;";
                MySqlCommand cmdComboSzakter = new MySqlCommand(comboszakter, conn);
                MySqlDataReader result = cmdComboSzakter.ExecuteReader();
                while (result.Read())
                {
                    comboszakter = result[0].ToString();
                    this.comboBoxSzakter.Items.Add(comboszakter);
                }
                result.Close();
            }
            catch (Exception error)
            {
                MessageBox.Show(Convert.ToString(error));
            }
            try
            {
                string combonyelvid = $"select id, fnyelv, cnyelv from nyelv;";
                MySqlCommand cmdcombonyelvid = new MySqlCommand(combonyelvid, conn);
                MySqlDataReader resultNyelv = cmdcombonyelvid.ExecuteReader();
                while (resultNyelv.Read())
                {
                    combonyelvid = resultNyelv[0] + "| " + resultNyelv[1].ToString() + "| " + resultNyelv[2].ToString();
                    this.comboBoxNyelvid.Items.Add(combonyelvid);
                }
                resultNyelv.Close();
            }
            catch (Exception error)
            {
                MessageBox.Show(Convert.ToString(error));
            }
        }
    }
}
