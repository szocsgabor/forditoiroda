﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Forditoiroda
{
    public partial class SzabadFordítok : Form
    {
        public static string connString = $"server=localhost;user=gabor;database=iroda;port=3306;password=gabor";
        MySqlConnection conn = new MySqlConnection(connString);
        public SzabadFordítok()
        {
            conn.Open();
            InitializeComponent();
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            label1.Visible = true;
            label1.Text = Convert.ToString("Fordítók akik munkát tudnak vállalni - abc sorrendben.");
            dataGridView1.DataSource = SzabadForditokListaja();
        }
        private object SzabadForditokListaja()
        {
            DataTable szabadok = new DataTable();
            string szabadforditok = $"select nev from szemely where szemely.elerheto='-1' order by nev asc;";
            MySqlCommand cmdnyelv = new MySqlCommand(szabadforditok, conn);
            MySqlDataReader rdr = cmdnyelv.ExecuteReader();
            szabadok.Load(rdr);
            rdr.Close();
            return szabadok;
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void SzabadFordítok_Load(object sender, EventArgs e)
        {
            label1.Visible = false;
        }
    }
}
