﻿namespace Forditoiroda
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelConnStatus = new System.Windows.Forms.Label();
            this.buttonExit = new System.Windows.Forms.Button();
            this.comboBoxSzemelyNev = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonSzemelyValaszt = new System.Windows.Forms.Button();
            this.buttonNewDoc = new System.Windows.Forms.Button();
            this.buttonUjMunka = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelConnStatus
            // 
            this.labelConnStatus.AutoSize = true;
            this.labelConnStatus.Enabled = false;
            this.labelConnStatus.Location = new System.Drawing.Point(49, 11);
            this.labelConnStatus.Name = "labelConnStatus";
            this.labelConnStatus.Size = new System.Drawing.Size(46, 17);
            this.labelConnStatus.TabIndex = 0;
            this.labelConnStatus.Text = "label1";
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(383, 394);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(135, 38);
            this.buttonExit.TabIndex = 1;
            this.buttonExit.Text = "Kilépés";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // comboBoxSzemelyNev
            // 
            this.comboBoxSzemelyNev.FormattingEnabled = true;
            this.comboBoxSzemelyNev.Location = new System.Drawing.Point(43, 84);
            this.comboBoxSzemelyNev.Name = "comboBoxSzemelyNev";
            this.comboBoxSzemelyNev.Size = new System.Drawing.Size(295, 24);
            this.comboBoxSzemelyNev.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Válasszon fordítót:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "label2";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.Color.Coral;
            this.dataGridView1.Location = new System.Drawing.Point(36, 158);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(440, 141);
            this.dataGridView1.TabIndex = 5;
            // 
            // buttonSzemelyValaszt
            // 
            this.buttonSzemelyValaszt.Location = new System.Drawing.Point(341, 84);
            this.buttonSzemelyValaszt.Name = "buttonSzemelyValaszt";
            this.buttonSzemelyValaszt.Size = new System.Drawing.Size(135, 68);
            this.buttonSzemelyValaszt.TabIndex = 6;
            this.buttonSzemelyValaszt.Text = "Szűrés";
            this.buttonSzemelyValaszt.UseVisualStyleBackColor = true;
            this.buttonSzemelyValaszt.Click += new System.EventHandler(this.ButtonSzemelyValaszt_Click);
            // 
            // buttonNewDoc
            // 
            this.buttonNewDoc.Location = new System.Drawing.Point(36, 394);
            this.buttonNewDoc.Name = "buttonNewDoc";
            this.buttonNewDoc.Size = new System.Drawing.Size(135, 38);
            this.buttonNewDoc.TabIndex = 7;
            this.buttonNewDoc.Text = "Új Dokumentum";
            this.buttonNewDoc.UseVisualStyleBackColor = true;
            this.buttonNewDoc.Click += new System.EventHandler(this.ButtonNewDoc_Click);
            // 
            // buttonUjMunka
            // 
            this.buttonUjMunka.Location = new System.Drawing.Point(205, 394);
            this.buttonUjMunka.Name = "buttonUjMunka";
            this.buttonUjMunka.Size = new System.Drawing.Size(133, 38);
            this.buttonUjMunka.TabIndex = 8;
            this.buttonUjMunka.Text = "Szabad fordítók";
            this.buttonUjMunka.UseVisualStyleBackColor = true;
            this.buttonUjMunka.Click += new System.EventHandler(this.ButtonUjMunka_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(520, 167);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(315, 132);
            this.listBox1.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 480);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.buttonUjMunka);
            this.Controls.Add(this.buttonNewDoc);
            this.Controls.Add(this.buttonSzemelyValaszt);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxSzemelyNev);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.labelConnStatus);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelConnStatus;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.ComboBox comboBoxSzemelyNev;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonSzemelyValaszt;
        private System.Windows.Forms.Button buttonNewDoc;
        private System.Windows.Forms.Button buttonUjMunka;
        private System.Windows.Forms.ListBox listBox1;
    }
}

