﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forditoiroda
{
    public partial class Form1 : Form
    {
        public static string connString = $"server=localhost;user=gabor;database=iroda;port=3306;password=gabor";
        MySqlConnection conn = new MySqlConnection(connString);
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //Betöltésre már csatlakozás:
            try
            {
                conn.Open();
                labelConnStatus.Enabled = true;
                labelConnStatus.Text = "Jelenleg Ön csatlakozik az adatbázishoz";
            }
            catch (Exception errormessage)
            {
                MessageBox.Show(Convert.ToString(errormessage));
                throw;
            }

            //Betöltésre a combobox feltöltése:
            try
            {
                string combo = "SELECT nev from szemely order by nev asc";
                MySqlCommand cmdcombo = new MySqlCommand(combo, conn);
                MySqlDataReader result = cmdcombo.ExecuteReader();
                while (result.Read())
                {
                    combo = result[0].ToString();
                    this.comboBoxSzemelyNev.Items.Add(combo);
                }
                result.Close();
            }
            catch (Exception error)
            {
                MessageBox.Show(Convert.ToString(error));
            }
            label2.Visible = false;
        }

        private void ButtonExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonSzemelyValaszt_Click(object sender, EventArgs e)
        {
            //Label megjelenítése és feltöltése a választott fordító nevével:
            label2.Visible = true;
            label2.Text = Convert.ToString(comboBoxSzemelyNev.SelectedItem);
            //DatagridView feltötlése a választott fordító nyelveivel.
            //dataGridView1.();
            dataGridView1.DataSource = NyelvekListaja();



            //feltöltjük a célnyelveket
            //try
            //{

            //    this.listBox1.Items.Clear();
            //    string res = this.comboBoxSzemelyNev.SelectedItem.ToString();
            //    string sql = $"SELECT nyelv.fnyelv AS 'Forrás nyelv', nyelv.cnyelv AS 'Célnyelv' FROM nyelv INNER JOIN fordito" +
            //        $" ON nyelv.id = fordito.nyelvid INNER JOIN szemely ON fordito.szemelyid = szemely.id WHERE szemely.nev = '{res}' GROUP BY cnyelv";
            //    MySqlCommand cmd = new MySqlCommand(sql, conn);
            //    MySqlDataReader result = cmd.ExecuteReader();
            //    while (result.Read())
            //    {
            //        sql = result[0].ToString() + " " + result[1].ToString();
            //        this.listBox1.Items.Add(sql);
            //    }

            //}
            //catch (Exception hiba)
            //{
            //    MessageBox.Show(hiba.ToString());
            //}








        }




            //Egy lista objectum a datagridview adatainak későbbi feltöltésére, visszatérési értéke a dgv tartalma:
            private object NyelvekListaja()
            {
                DataTable nyelvek = new DataTable();
                string valasztottNev = Convert.ToString(comboBoxSzemelyNev.SelectedItem);
                string valasztottnyelvek = $"select n.fnyelv as 'FORRÁS NYELV', n.cnyelv as 'CÉL NYELV' from szemely sz, fordito f, nyelv n where n.id = f.nyelvid and sz.id = f.szemelyid  AND sz.nev ='{valasztottNev}'; ";
                MySqlCommand cmdnyelv = new MySqlCommand(valasztottnyelvek, conn);
                MySqlDataReader rdr = cmdnyelv.ExecuteReader();
                nyelvek.Load(rdr);
                rdr.Close();
                return nyelvek;
            }
            private void ButtonNewDoc_Click(object sender, EventArgs e)
            {
                Doku dokumentum = new Doku();
                dokumentum.ShowDialog();
            }
            private void ButtonUjMunka_Click(object sender, EventArgs e)
            {
                SzabadFordítok szabad = new SzabadFordítok();
                szabad.ShowDialog();
            }
        }
    }
