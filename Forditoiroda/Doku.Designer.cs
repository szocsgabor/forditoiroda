﻿namespace Forditoiroda
{
    partial class Doku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonDocuExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxSzakter = new System.Windows.Forms.ComboBox();
            this.comboBoxNyelvid = new System.Windows.Forms.ComboBox();
            this.textBoxTerjed = new System.Windows.Forms.TextBox();
            this.textBoxMunkaido = new System.Windows.Forms.TextBox();
            this.buttonDokuInsert = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonDocuExit
            // 
            this.buttonDocuExit.Location = new System.Drawing.Point(207, 382);
            this.buttonDocuExit.Name = "buttonDocuExit";
            this.buttonDocuExit.Size = new System.Drawing.Size(112, 45);
            this.buttonDocuExit.TabIndex = 0;
            this.buttonDocuExit.Text = "Kilépés";
            this.buttonDocuExit.UseVisualStyleBackColor = true;
            this.buttonDocuExit.Click += new System.EventHandler(this.ButtonDocuExit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Terjedelem";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Szakterület";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 21);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nyelv";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 21);
            this.label4.TabIndex = 4;
            this.label4.Text = "Munkaidő";
            // 
            // comboBoxSzakter
            // 
            this.comboBoxSzakter.FormattingEnabled = true;
            this.comboBoxSzakter.Location = new System.Drawing.Point(160, 75);
            this.comboBoxSzakter.Name = "comboBoxSzakter";
            this.comboBoxSzakter.Size = new System.Drawing.Size(159, 24);
            this.comboBoxSzakter.TabIndex = 5;
            // 
            // comboBoxNyelvid
            // 
            this.comboBoxNyelvid.FormattingEnabled = true;
            this.comboBoxNyelvid.Location = new System.Drawing.Point(160, 122);
            this.comboBoxNyelvid.Name = "comboBoxNyelvid";
            this.comboBoxNyelvid.Size = new System.Drawing.Size(159, 24);
            this.comboBoxNyelvid.TabIndex = 6;
            // 
            // textBoxTerjed
            // 
            this.textBoxTerjed.Location = new System.Drawing.Point(160, 30);
            this.textBoxTerjed.Name = "textBoxTerjed";
            this.textBoxTerjed.Size = new System.Drawing.Size(159, 22);
            this.textBoxTerjed.TabIndex = 7;
            // 
            // textBoxMunkaido
            // 
            this.textBoxMunkaido.Location = new System.Drawing.Point(160, 170);
            this.textBoxMunkaido.Name = "textBoxMunkaido";
            this.textBoxMunkaido.Size = new System.Drawing.Size(159, 22);
            this.textBoxMunkaido.TabIndex = 8;
            // 
            // buttonDokuInsert
            // 
            this.buttonDokuInsert.Location = new System.Drawing.Point(160, 270);
            this.buttonDokuInsert.Name = "buttonDokuInsert";
            this.buttonDokuInsert.Size = new System.Drawing.Size(159, 45);
            this.buttonDokuInsert.TabIndex = 9;
            this.buttonDokuInsert.Text = "Rögzít";
            this.buttonDokuInsert.UseVisualStyleBackColor = true;
            this.buttonDokuInsert.Click += new System.EventHandler(this.ButtonDokuInsert_Click);
            // 
            // Doku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(402, 450);
            this.Controls.Add(this.buttonDokuInsert);
            this.Controls.Add(this.textBoxMunkaido);
            this.Controls.Add(this.textBoxTerjed);
            this.Controls.Add(this.comboBoxNyelvid);
            this.Controls.Add(this.comboBoxSzakter);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonDocuExit);
            this.Name = "Doku";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Új dokumentum felvétele";
            this.Load += new System.EventHandler(this.Doku_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDocuExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxSzakter;
        private System.Windows.Forms.ComboBox comboBoxNyelvid;
        private System.Windows.Forms.TextBox textBoxTerjed;
        private System.Windows.Forms.TextBox textBoxMunkaido;
        private System.Windows.Forms.Button buttonDokuInsert;
    }
}